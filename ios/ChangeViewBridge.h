//
//  ChangeViewBridge.h
//  HelloWorld
//
//  Created by Emre Kuru on 14.06.2021.
//

#import <React/RCTBridgeModule.h>

@interface ChangeViewBridge : NSObject <RCTBridgeModule>

- (void) changeToNativeView;

@end
