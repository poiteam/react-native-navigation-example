//
//  PLSuspendedAnalysisManager.h
//  PoilabsAnalysis
//
//  Created by ERCAN AYYILDIZ on 08/08/2017.
//  Copyright © 2017 poilabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PLSuspendedAnalysisManager : NSObject

+ (instancetype)sharedInstance;

-(void)startBeaconMonitoring;

-(void)stopBeaconMonitoring;

-(void)bluetoothStatusChanged:(BOOL)bluetoothStatus;

@end
