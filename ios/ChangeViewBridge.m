//
//  ChangeViewBridge.m
//  HelloWorld
//
//  Created by Emre Kuru on 14.06.2021.
//

#import <Foundation/Foundation.h>
//#import "RegisterBridge.h"
//#import "FidoTestProject-Swift.h"
#import "ChangeViewBridge.h"
#import "AppDelegate.h"

@implementation ChangeViewBridge : NSObject 

// reference "ChangeViewBridge" module in index.ios.js
RCT_EXPORT_MODULE(ChangeViewBridge);

RCT_EXPORT_METHOD(changeToNativeView) {
  NSLog(@"RN binding - Native View - Loading MyViewController.swift");
  dispatch_async(dispatch_get_main_queue(), ^{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate goToRegisterView];
    
  });

}

@end
