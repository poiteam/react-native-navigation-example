//
//  ViewController.swift
//  HelloWorld
//
//  Created by Emre Kuru on 14.06.2021.
//

import UIKit
import PoilabsNavigation

class ViewController: UIViewController {

  @IBOutlet weak var navigationView: UIView!
  var currentCarrier: PLNNavigationMapView?
  
  override func viewDidLoad() {
        super.viewDidLoad()
    self.view.backgroundColor = .red 
    PLNNavigationSettings.sharedInstance().mallId = "mallId"
    PLNNavigationSettings.sharedInstance().navigationUniqueIdentifier = "01BurdaNavigation"
    PLNNavigationSettings.sharedInstance().applicationId = "d0622739-f99a-4080-a6c6-dd1b5a7a6582"
    PLNNavigationSettings.sharedInstance().applicationSecret = "71055695-3559-4694-89c8-18037034c72d"
    
    PLNavigationManager.sharedInstance().getReadyForStoreMap { (error) in
        if (error != nil) {
            print(error?.errorDescription ?? "Error")

        }
        else {

          let carrierView = PLNNavigationMapView(frame: CGRect(x: 0, y: 0, width: self.navigationView.bounds.size.width, height: self.navigationView.bounds.size.height))
          carrierView.awakeFromNib()
          carrierView.delegate = self as? PLNNavigationMapViewDelegate
          carrierView.searchBarBaseView.backgroundColor = UIColor.black
          self.currentCarrier = carrierView
          self.navigationView.addSubview(carrierView)
        }

    }
    }
  
  @IBAction func dismiss(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
  }
  

}
